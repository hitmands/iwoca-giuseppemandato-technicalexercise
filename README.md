# Giuseppe Mandato - Iwoca Technical Exercise

### Getting Started

```bash
  git clone https://hitmands@bitbucket.org/hitmands/iwoca-giuseppemandato-technicalexercise.git
  cd iwoca-giuseppemandato-technicalexercise
  
  yarn install --frozen-lockfile
  
  yarn test
  
  yarn build
  
  yarn start
```

### Music Player Project - Requirements

#### Introduction 
Create a front-end project that simulates the web interface of a music player (think Spotify).

#### Features
  - Show a list of available songs (title, artist, duration). The list can be hard-coded client
side.
  - Show a Play/Pause button and highlight the song in the list when it’s selected
  - Display the remaining duration of the currently playing song in real time

#### What we are looking for
  - Code quality and structure
  - Consideration of UI/UX

#### Technical requirements:
  - There are no requirements for the tech stack. Vanilla JavaScript is fine, as is any
framework that you feel is appropriate.
  - There’s no need to play any audio file… but if you think that you have great taste in
music feel free to show us ;-)
  - Try and spend no more than 4 hours on it!
