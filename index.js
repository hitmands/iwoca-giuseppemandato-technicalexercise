const path = require('path');
const express = require('express');
const expressStatic = require('express-static');
const { name, version } = require('./package');


// eslint-disable-next-line no-console
const log = stats => console.log(`
application(${name}@${version}).Start(${JSON.stringify(stats, null, 2)});
`);

const env = process.env.NODE_ENV || 'development';
const port = process.env.NODE_PORT || 3002;
const index = path.resolve('./src/application.html');
const dist = path.resolve('./dist');

express()
  .use('/assets', expressStatic(dist, {}))
  .get('/*', (req, res) => (
    res
      .header('Content-Type', 'text/html')
      .sendFile(index)
  ))
  .listen(port, () => {
    log({
      address: `https://localhost:${port}`,
      environment: env,
    });
  });
