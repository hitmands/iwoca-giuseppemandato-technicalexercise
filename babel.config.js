const presets = [
  ['@babel/preset-react', {}],
  ['@babel/preset-typescript', {}],
  ['@babel/preset-env', { useBuiltIns: 'entry', targets: { node: 'current' } }],
];

const plugins = [
  ['@babel/plugin-transform-runtime'],
  ['babel-plugin-styled-components', { ssr: true, pure: true }],
  ['graphql-tag'],
  ['import-graphql'],
  ['@babel/plugin-proposal-object-rest-spread', {}],
];

// @babel/preset-env overwritten in webpack.config.js
module.exports = { presets, plugins };
