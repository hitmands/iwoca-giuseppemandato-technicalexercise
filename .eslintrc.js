module.exports = {
  extends: ['airbnb'],
  env: {
    jest: true,
    browser: true,
  },
  plugins: [
    'jest',
  ],
  rules: {
    'import/prefer-default-export': 0,
    'import/no-default-export': 2,
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    "react/jsx-filename-extension": [2, { "extensions": [".js", ".jsx"] }],

  }
};
