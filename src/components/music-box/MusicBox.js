import React from 'react';
import { Query } from 'react-apollo';
import styled from 'styled-components';
import { pipe, withState, withProps } from '@synvox/rehook';
import { useMedia } from 'use-media';
import { FETCH_SONGS } from '../../api/songs';
import { SongsList } from '../songs-list';
import { Controls } from '../controls';
import { YoutubePlayer } from '../youtube-player';


const fetchSongVars = { query: 'favourites' };

const Container = styled.section`
  display: flex;
  flex-direction: column; 
  height: 100%;
  position: relative;
  
  @media (min-width: 1024px) {
    max-height: 360px;
    flex-direction: row;
  }
`;

const Sidebar = styled.aside`
  flex: 0 0 140px;
  max-height: 100%;
  overflow: auto;
  background-color: #142929;
  
  @media (min-width: 1024px) {
    flex-basis: 360px;  
  }
`;

const Main = styled.article`
  display: flex;
  flex-direction: column;
  flex: 0 0 500px;
  justify-content: center;
  position: relative;
  
  @media (min-width: 1024px) {
    flex-basis: 640px;  
  }
`;

const hooks = pipe(
  withState('song', 'setSong'),
  withState('progress', 'setProgress'),
  withState('duration', 'setDuration'),
  withState('playing', 'setPlaying', true),
  withProps(props => ({
    play: () => props.setPlaying(true),
    pause: () => props.setPlaying(false),
    setCurrentSong: song => (event) => {
      event.preventDefault();

      props.setProgress(null);
      props.setPlaying(true);
      props.setSong(song);
    },
  })),
);

export const MusicBox = () => {
  const isViewportSmall = useMedia('(max-width: 1023px)');
  const {
    duration,
    pause,
    play,
    playing,
    progress,
    setCurrentSong,
    setDuration,
    setProgress,
    song,
  } = hooks();

  return (
    <Query query={FETCH_SONGS} variables={fetchSongVars}>
      {({ loading, data }) => (loading ? null : (
        <Container>
          <Sidebar>
            <SongsList
              data={data}
              active={song}
              setCurrentSong={setCurrentSong}
            />
          </Sidebar>

          <Main xs={isViewportSmall}>
            <YoutubePlayer
              xs={isViewportSmall}
              song={song}
              onProgress={setProgress}
              playing={playing}
              onPlay={play}
              onPause={pause}
              onDuration={setDuration}
            />

            <Controls
              song={song}
              duration={duration}
              progress={progress}
              playing={playing}
              play={play}
              pause={pause}
              onEnded={pause}
            />
          </Main>
        </Container>
      ))}
    </Query>
  );
};
