import { mount } from 'enzyme';
import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import { MusicBox } from './MusicBox';
import { data } from '../../api/definition';


describe('Youtube Player', () => {
  it('should match snapshot', () => {
    expect(
      mount(
        <MockedProvider mocks={data} addTypename={false}>
          <MusicBox />
        </MockedProvider>,
      ),
    ).toMatchSnapshot();
  });
});
