import React from 'react';
import {
  shape, arrayOf, func, string,
} from 'prop-types';
import styled from 'styled-components';
import { SongPropType } from '../../api/definition';


const List = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`;

const Song = styled.li`
  cursor: pointer;
  opacity: .6;
  padding: 5px;
  transition: 350ms all linear;
  
  &[aria-selected="true"] {
    background-color: azure;
    color: #141414;
    opacity: 1;
    pointer-events: none;
  }
  
  &:hover {
    opacity: 1;
  }
`;

const Title = styled.a`
  color: inherit;
  display: block;
  font-size: 1em;
  margin: 0;
  padding: .5em 1em;
  text-decoration: none;
  
  small {
    border-top: 1px solid;
    display: block;
    margin-top: 2px;
    padding-top: 2px;
  }
`;


export const SongsList = ({ data: { songs }, setCurrentSong, active }) => (
  <List
    role="tablist"
    aria-label="Giuseppe Mandato's favourites songs"
  >
    {
      songs.map((song, index) => (
        <Song
          key={song.id}
          aria-selected={song.id === active.id}
        >
          <Title
            role="tab"
            id={`link-to-${song.id}`}
            href={`/?v=${song.id}`}
            onClick={setCurrentSong(song)}
            aria-selected={song.id === active.id}
            aria-controls={`tab-${song.id}`}
            aria-label={
              `${
                active.id === song.id ? 'press enter to play' : 'You selected'
              }: ${song.title}, author: ${song.author}, year: ${song.year}`
            }
          >
            {index + 1}
            {' '}
-
            {song.title}

            <small>
              {song.author}
,
              {' '}
              {song.year}
            </small>
          </Title>
        </Song>
      ))
    }
  </List>
);

SongsList.defaultProps = {
  active: { id: null },
};

SongsList.propTypes = {
  data: shape({ songs: arrayOf(SongPropType) }).isRequired,
  active: shape({ id: string }),
  setCurrentSong: func.isRequired,
};
