import { mount } from 'enzyme';
import React from 'react';
import { SongsList } from './SongsList';


const songs = [
  {
    title: 'Futura',
    id: 'JGmPdb0wjQc',
    author: 'Lucio Dalla',
    duration: 6.06 * 60,
    year: 1980,
  },
];

describe('Youtube Player', () => {
  it('should match snapshot', () => {
    expect(
      mount(
        <SongsList
          data={{ songs }}
          setCurrentSong={() => () => 'hello world'}
        />,
      ),
    ).toMatchSnapshot();
  });

  it('[selected song] should match snapshot', () => {
    expect(
      mount(
        <SongsList
          data={{ songs }}
          setCurrentSong={() => () => 'hello world'}
          active={songs[0]}
        />,
      ),
    ).toMatchSnapshot();
  });
});
