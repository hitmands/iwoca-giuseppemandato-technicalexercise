import { bool, string, shape } from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import ReactPlayer from 'react-player';


const DefaultText = styled.p`
  font-size: 1.5em;
  text-align: center;
  text-transform: uppercase;
  user-select: none;
`;

export const YoutubePlayer = ({ song, xs, ...rest }) => (song
  ? (
    <ReactPlayer
      id={`tab-${song.id}`}
      url={`https://www.youtube.com/watch?v=${song.id}`}
      controls={false}
      playsinline
      progressInterval={500}
      width={xs ? 360 : 640}
      height={xs ? 500 : 360}
      {...rest}
    />
  )
  : (
    <DefaultText>
      Please select your today&apos;s italian song
      <br />
      from the list on the left.
    </DefaultText>
  ));

YoutubePlayer.defaultProps = {
  song: null,
  xs: false,
};

YoutubePlayer.propTypes = {
  song: shape({ id: string }),
  xs: bool,
};
