import { mount } from 'enzyme';
import React from 'react';
import { YoutubePlayer } from './YoutubePlayer';


describe('Youtube Player', () => {
  it('should render default message if {!song}', () => {
    expect(
      mount(<YoutubePlayer song={null} />),
    ).toMatchSnapshot();
  });

  it('should render player accordingly with {song}', () => {
    const song = {
      title: 'hello world',
      id: 'hello-world',
    };

    expect(
      mount(<YoutubePlayer song={song} />),
    ).toMatchSnapshot();
  });
});
