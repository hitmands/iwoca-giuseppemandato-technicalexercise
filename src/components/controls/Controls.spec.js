import { mount } from 'enzyme';
import React from 'react';
import { Controls, toMinutes } from './Controls';


const song = {
  title: 'Futura',
  id: 'JGmPdb0wjQc',
  author: 'Lucio Dalla',
  duration: 6.06 * 60,
  year: 1980,
};

describe('Youtube Player', () => {
  it('{toMinutes}', () => {
    [
      [120, '02:00'],
      [340, '05:40'],
      [823, '13:43'],
      [4, '00:04'],
    ].forEach(([given, expected]) => {
      expect(toMinutes(given)).toEqual(expected);
    });
  });

  it('[paused] should match snapshot', () => {
    expect(
      mount(
        <Controls
          progress={{ playedSeconds: 100 }}
          duration={120}
          song={song}
          playing={false}
          play={() => 'play'}
          pause={() => 'pause'}
        />,
      ),
    ).toMatchSnapshot();
  });

  it('[playing] should match snapshot', () => {
    expect(
      mount(
        <Controls
          progress={{ playedSeconds: 140 }}
          duration={258}
          song={song}
          playing
          play={() => 'play'}
          pause={() => 'pause'}
        />,
      ),
    ).toMatchSnapshot();
  });
});
