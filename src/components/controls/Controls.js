import React from 'react';
import {
  shape, number, func, bool,
} from 'prop-types';
import styled, { keyframes } from 'styled-components';
import { SongPropType } from '../../api/definition';

const Backdrop = styled.div`
  bottom: 0;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  user-select: none;
`;

const enter = keyframes`
  from { opacity: 0 }
  to { opacity: 100 }
`;

const Container = styled.footer`
  align-items: center;
  animation-iteration-count: 1;
  animation: ${enter} 250ms linear;
  background-color: azure;
  bottom: 0;
  color: black;
  display: flex;
  justify-content: space-between;
  padding: 1.30em 2em;
  position: absolute;
  width: 100%;
`;

const Time = styled.div`
  pointer-events: none;
  white-space: nowrap;
    
  small {
    display: inline-block;
    padding-left: .5em;
  }
`;

const Button = styled.button`
  background: none;
  border: none;
  cursor: pointer;
  width: 2em;
`;

export const toMinutes = seconds => [
  Math.floor(seconds / 60).toString().padStart(2, '0'),
  Math.floor(seconds % 60).toString().padStart(2, '0'),
].join(':');

export const Controls = ({
  progress, duration, song, play, pause, playing,
}) => (
  <Backdrop>
    {
      progress ? (
        <Container>
          <Time>
            {toMinutes(duration - progress.playedSeconds)}
            <small>
              {song.title}
              {' '}
(
              {song.author}
,
              {' '}
              {song.year}
)
            </small>
          </Time>

          <Button
            onClick={playing ? pause : play}
            dangerouslySetInnerHTML={{ __html: playing ? '&#10073;&#10073;' : '&#9658;' }}
            aria-label={`press enter to ${playing ? 'pause' : 'play'} ${song.title}`}
          />
        </Container>
      ) : null
    }
  </Backdrop>
);

Controls.defaultProps = {
  progress: null,
  duration: null,
  song: null,
};

Controls.propTypes = {
  play: func.isRequired,
  pause: func.isRequired,
  playing: bool.isRequired,
  duration: number,
  progress: shape({ playedSeconds: number.isRequired }),
  song: SongPropType,
};
