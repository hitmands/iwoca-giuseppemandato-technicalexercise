import React, { Fragment } from 'react';
import styled, { createGlobalStyle } from 'styled-components';


const GlobalStyle = createGlobalStyle`
  *,
  *::after,
  *::before {
    box-sizing: border-box;
  }
  
  html {
    font-family: Helvetica Neue, sans-serif;
    font-size: 16px;
    background-color: #141414;
    color: azure;
    min-width: 360px;
  }
`;

const Container = styled.div`
  background-color: black;
  box-shadow: 8px 8px 5px -6px rgba(0, 0, 0, .7);
  margin: 0 auto;
  overflow: hidden;
  
  @media (min-width: 1024px) {
    margin-top: 3em;
    width: 1000px;  
    border-radius: 12px;
  } 
`;

export const Layout = props => (
  <Container>
    <GlobalStyle />

    <Fragment {...props} />
  </Container>
);
