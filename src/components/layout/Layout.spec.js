import { mount } from 'enzyme';
import React from 'react';
import { Layout } from './Layout';


describe('Layout', () => {
  it('should match snapshot', () => {
    expect(
      mount(<Layout />),
    ).toMatchSnapshot();
  });
});
