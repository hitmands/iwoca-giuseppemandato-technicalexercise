import gql from 'graphql-tag';


export const FETCH_SONGS = gql`
  query fetchSongs($query: String!) {
    songs(query: $query) {
      id
      title
      author
      year
    }
  }
`;
