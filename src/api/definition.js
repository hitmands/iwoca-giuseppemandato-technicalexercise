import { shape, number, string } from 'prop-types';
import { FETCH_SONGS } from './songs';


const songs = [
  {
    title: 'Futura',
    id: 'JGmPdb0wjQc',
    author: 'Lucio Dalla',
    year: 1980,
  },
  {
    title: 'Titanic',
    id: 'BhYhqZZxvKA',
    author: 'Francesco De Gregori',
    year: 1982,
  },
  {
    title: 'Coda Di Lupo',
    id: 'd7at7p--lKI',
    author: 'Fabrizio De Andrè',
    year: 1978,
  },
  {
    title: 'Vorrei',
    id: '6Gf5qrhb9n8',
    author: 'Francesco Guccini',
    year: 1996,
  },
  {
    title: 'Ma il cielo è sempre più blu',
    id: 'G8ioOG-PaxQ',
    author: 'Rino Gaetano',
    year: 1975,
  },
  {
    title: 'Si può',
    id: 'A1i_nmPN6SI',
    author: 'Giorgio Gaber',
    year: 1992,
  },
  {
    title: 'La Favola di Adamo ed Eva',
    id: 'BH51lx2zk54',
    author: 'Max Gazzè',
    year: 1998,
  },
  {
    title: 'Nuvole senza Messico',
    id: 'JbQoC_H2oC0',
    author: 'Giorgio Canali e Rossofuoco',
    year: 2009,
  },
];

export const data = [
  {
    request: {
      query: FETCH_SONGS,
      variables: { query: 'favourites' },
    },
    result: {
      data: { songs },
    },
  },
];

export const SongPropType = shape({
  id: string.isRequired,
  author: string.isRequired,
  year: number.isRequired,
  title: string.isRequired,
});
