import React from 'react';
import { MockedProvider as ApolloProvider } from 'react-apollo/test-utils';
import { render } from 'react-dom';
import { Layout } from './components/layout';
import { MusicBox } from './components/music-box';
import { data } from './api/definition';


render((
  <ApolloProvider mocks={data} addTypename={false}>
    <Layout>
      <MusicBox />
    </Layout>
  </ApolloProvider>
), document.querySelector('[data-application-root]'));
