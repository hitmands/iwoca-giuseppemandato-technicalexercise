module.exports = {
  collectCoverage: true,
  setupTestFrameworkScriptFile: '<rootDir>/jest.framework.config.js',
  snapshotSerializers: [
    '<rootDir>/node_modules/enzyme-to-json/serializer',
  ],
  roots: [
    '<rootDir>/src',
  ],
  testMatch: [
    '**/*.spec.js',
  ],
  transform: {
    '^.+\\.js$': 'babel-jest',
  },
  moduleFileExtensions: [
    'js',
    'json',
  ],
  globals: {
    __WEBPACK_THEMOVIEDATABASE_API_KEY__: 'webpack_themoviedatabase_api_key',
    __WEBPACK_ROOT__: 'webpack_mock_root',
    __WEBPACK_DIST__: 'webpack_mock_dist',
    __WEBPACK_PUBLIC_ASSETS_PATH__: 'webpack_mock_assets',
    __BUILD_DATE__: 'webpack_mock_build-date',
    __WEBPACK_TARGET__: 'webpack_mock_target',
    __WEBPACK_TARGET_IS_WEB__: false,
    __WEBPACK_TARGET_IS_NODE__: false,
    __PROJECT_NAME__: 'webpack_mock_project_name',
    __PROJECT_VERSION__: 'webpack_mock_project_version',
    __PROJECT_AUTHOR__: 'webpack_mock_project_author',
  },
  collectCoverageFrom: [
    '<rootDir>/src/**/*.js',
    '!<rootDir>/src/**/index.js',
    '!**/node_modules/**',
    '!**/vendor/**',
  ],
  coverageReporters: [
    'html',
  ],
};
