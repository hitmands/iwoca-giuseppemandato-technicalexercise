const path = require('path');
const webpack = require('webpack');
const { author, license, version, name } = require('./package');


const env = process.env.NODE_ENV || 'development';

const babel = () => ({
  test: /\.m?js$/,
  exclude: /(node_modules|bower_components)/,
  use: {
    loader: 'babel-loader',
    options: {
      overrides: [
        {
          presets: [['@babel/preset-env', {
            targets: '> 0.25%, not dead',
            useBuiltIns: 'entry',
            modules: false,
          }]],
        },
      ],
    },
  },
});

const withBase = (...args) => Object.assign({}, {
  mode: env,
  devtool: 'source-map',
  output: { filename: '[name].js', path: path.join(__dirname, 'dist') },
  module: { rules: [babel()] }
}, ...args);

module.exports = withBase({
  target: 'web',
  mode: env,
  context: path.join(__dirname, 'src'),
  entry: {
    application: ['./application.js'],
  },
  plugins: [
    new webpack.BannerPlugin({
      raw: true,
      entryOnly: true,
      banner: `/**!
 * @project ${name}@${version}
 * @author ${author}
 * @license ${license} 
 * @build: ${new Date()} | ${env}
**/`
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(env),
      '__SPOTIFY_API_KEY__': 'xxx',
    }),
  ],
});
